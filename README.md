## <h3>Soundcheck</h3>

<h6>Fast soundcloud email validator</h6>

---

<p>
    <img src="https://cdn.discordapp.com/attachments/1065385280393203892/1217912789146800320/image.png?ex=6605c0ac&is=65f34bac&hm=bad3dfaeadee27621487e72b0a8fdb1f64bd9af7d5e1a5c0b49bcac71b55be2f&" alt="preview" width="400px"/>
</p>

---

<h4>Introduction</h4>

- Soundcheck was made to verify if an email was used on soundcloud
- Useful for scammer who wants valid email list

---

<h4>Setting-Up</h4>

- Install Golang
- Install library (go mod tidy)
- Place proxy in list/proxies.txt if you have
- At source (soundcheck/) execute: go run src/main.go

---

<h4>Features</h4>

- Fast hits
- Multithreads
- Debug mode (can be enable in config.go)
- Proxyless (works well)
- Proxy Support (HTTPS, SOCKS4, SOCKS5)
- Save everything in files

---

<h4>Warning</h4>

- This project was made for educational purposes only! I take no responsibility for anything you do with this program.
- If you have any suggestions, problems, open a problem (if it is an error, you must be sure to look if you can solve it with [Google](https://giybf.com)!)

<h4>Support me</h4>

- Thanks for looking at this repository, you can donate me btc to `bc1q0jc0dd6a7alzmr8j7hseg6r5d8333re9wu87wj`
- Made by [zoloft](https://gitlab.com/zoloft).

<div align="center">
    <b>Informations</b><br>
    <img alt="GitHub Repo stars" src="https://img.shields.io/github/stars/imzoloft/soundcheck?color=000">
    <img alt="GitHub top language" src="https://img.shields.io/github/languages/top/imzoloft/soundcheck?color=000">
    <img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/imzoloft/soundcheck?color=000">
    <img alt="GitHub" src="https://img.shields.io/github/license/imzoloft/soundcheck?color=000">
    <img alt="GitHub watchers" src="https://img.shields.io/github/watchers/imzoloft/soundcheck?color=000">
</div>
